﻿namespace Marathon.Models.Directories;

public interface IBaseSortOptions<TSortState>
    where TSortState : System.Enum
{
    TSortState OrderBy { get; set; }
}

public interface IBasePaginationOptions
{
    bool IsInfiniteScroll { get; set; }
    int LoadedCount { get; set; }
    int Page { get; set; }
    int Size { get; set; }
}

public class BaseSortOptions<TSortState> : IBaseSortOptions<TSortState>
    where TSortState : System.Enum
{
    public TSortState OrderBy { get; set; }

    public BaseSortOptions() { }

    public BaseSortOptions(TSortState defaultOrderBy)
    {
        this.OrderBy = defaultOrderBy;
    }
}

public class BasePaginationOptions<TSortState> : BaseSortOptions<TSortState>, IBasePaginationOptions
    where TSortState : System.Enum
{
    public BasePaginationOptions() { }

    public BasePaginationOptions(TSortState defaultOrderBy)
        : base(defaultOrderBy) { }

    public BasePaginationOptions(int size, int page, TSortState defaultOrderBy)
        : base(defaultOrderBy)
    {
        Size = size;
        Page = page;
    }

    public bool IsInfiniteScroll { get; set; }
    public int LoadedCount { get; set; }
    public int Page { get; set; } = 1;
    public int Size { get; set; } = 10;
}