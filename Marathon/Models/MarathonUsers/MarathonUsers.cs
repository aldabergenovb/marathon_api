﻿namespace Marathon.Models.MarathonUsers;

public class MarathonUsers
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public DateTime CreatedDate { get; set; } = DateTime.Now;
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime BirthDate { get; set; }
    public string GenderName { get; set; }
    public string CountryName { get; set; }
    public string CityName { get; set; }
    public string ExtraContact { get; set; }
    public string Size { get; set; }
    public string Distance { get; set; }
    public bool IsPaid { get; set; }
}

public class MarathonUsersCreateViewModel
{
    public DateTime CreatedDate { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime BirthDate { get; set; }
    public string GenderName { get; set; }
    public string CountryName { get; set; }
    public string CityName { get; set; }
    public string ExtraContact { get; set; }
    public string Size { get; set; }
    public string Distance { get; set; }
}

public class MarathonUsersPutViewModel
{
    public Guid Id { get; set; }
    public DateTime CreatedDate { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime BirthDate { get; set; }
    public string GenderName { get; set; }
    public string CountryName { get; set; }
    public string CityName { get; set; }
    public string ExtraContact { get; set; }
    public string Size { get; set; }
    public string Distance { get; set; }
    public bool IsPaid { get; set; }
}