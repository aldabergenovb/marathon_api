﻿using AutoMapper;
using Marathon.Models.MarathonUsers;

namespace Marathon.Models;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<MarathonUsers.MarathonUsers, MarathonUsersPutViewModel>();
    }
}