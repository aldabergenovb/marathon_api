﻿using Marathon.Constants;
using Marathon.Filters;
using Marathon.Models.Directories;
using Marathon.Models.MarathonUsers;
using Marathon.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Marathon.Controllers;

public class MarathonUserController : Controller
{
    private readonly IMarathonUserRepository _repo;

    public MarathonUserController(IMarathonUserRepository repo)
    {
        _repo = repo;
    }
    
    /// <summary>
    /// Получить список всех марафонцев
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    [HttpGet("getAllPaginated")]
    // [Authorize(AuthenticationSchemes = "Digest")]
    public async Task<IActionResult> GetAllPaginatedAsync([FromQuery] MarathonUserFilters options)
    {
        ActionMethodResult result;
        if (ModelState.IsValid)
            result = await _repo.GetAllPaginatedAsync(options);
        else
            result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", "Ошибка");
        return Ok(result);
    }

    /// <summary>
    /// Получить одного пользователя
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    // [Authorize(AuthenticationSchemes = "Digest")]
    public async Task<IActionResult> GetAsync(Guid id)
    {
        ActionMethodResult result;
        if (!id.Equals(Guid.Empty))
            result = await _repo.GetAsync(id);
        else
            result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", "Ошибка");
        return Ok(result);
    }

    /// <summary>
    /// Создать новую запись
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("create")]
    // [Authorize(AuthenticationSchemes = "Digest")]
    public async Task<IActionResult> CreateAsync([FromBody] MarathonUsersCreateViewModel model)
    {
        ActionMethodResult result;
        if (ModelState.IsValid)
            result = await _repo.CreateAsync(model);
        else
            result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", "Системная ошибка");
        return Ok(result);
    }

    /// <summary>
    /// Изменение данных
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut("put")]
    // [Authorize(AuthenticationSchemes = "Digest")]
    public async Task<IActionResult> PutAsync([FromBody] MarathonUsersPutViewModel model)
    {
        ActionMethodResult result;
        if (ModelState.IsValid)
            result = await _repo.PutAsync(model);
        else
            result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", "Системная ошибка");
        return Ok(result);
    }
}