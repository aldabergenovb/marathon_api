﻿using System.Security.Claims;
using AutoMapper;
using Marathon.Constants;
using Marathon.Filters;
using Marathon.Helpers;
using Marathon.Migrations;
using Marathon.Models;
using Marathon.Models.Directories;
using Marathon.Models.MarathonUsers;
using Marathon.Services;
using Microsoft.EntityFrameworkCore;

namespace Marathon.Repositories;

public interface IMarathonUserRepository
{
    Task<ActionMethodResult> GetAsync(Guid id);
    Task<ActionMethodResult> CreateAsync(MarathonUsersCreateViewModel model);
    Task<ActionMethodResult> GetAllPaginatedAsync(MarathonUserFilters options);
    Task<ActionMethodResult> PutAsync(MarathonUsersPutViewModel model);
}

public class MarathonUserRepository : IMarathonUserRepository
{
    private readonly AppDbContext _db;
    private readonly IMapper _mapper;

    public MarathonUserRepository(AppDbContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }

    public async Task<ActionMethodResult> GetAsync(Guid id)
    {
        var users = await _db.MarathonUsers.FirstOrDefaultAsync(x => x.Id == id);
        if (users is null)
            return ActionMethodResult.Error(null, "Данные не найдены");
        return ActionMethodResult.Success(users);
    }
    
    public async Task<ActionMethodResult> CreateAsync(MarathonUsersCreateViewModel model)
    {
        MarathonUsers user = new MarathonUsers
        {
            CreatedDate = model.CreatedDate,
            Email = model.Email,
            PhoneNumber = model.PhoneNumber,
            FirstName = model.FirstName,
            LastName = model.LastName,
            BirthDate = model.BirthDate,
            GenderName = model.GenderName,
            CountryName = model.CountryName,
            CityName = model.CityName,
            ExtraContact = model.ExtraContact,
            Size = model.Size,
            Distance = model.Distance
        };

        await _db.MarathonUsers!.AddAsync(user);
        await _db.SaveChangesAsync();

        return ActionMethodResult.Success(user.Id, "Успешно сохранено");
    }
    
    public async Task<ActionMethodResult> PutAsync(MarathonUsersPutViewModel model)
    {
        var marathonUser = _db.MarathonUsers!.FirstOrDefault(x => x.Id == model.Id);

        if (marathonUser != null)
        {
            marathonUser.IsPaid = model.IsPaid;
        }

        // EmailSender emailSender = new EmailSender();
        // emailSender.SendEmail("test", "test", "test");
        _db.MarathonUsers?.Update(marathonUser!);
        await _db.SaveChangesAsync();

        return ActionMethodResult.Success(marathonUser.Id, "Успешно сохранено");
    }
    
    public async Task<ActionMethodResult> GetAllPaginatedAsync(MarathonUserFilters options)
    {
        IQueryable<MarathonUsers> users = _db.MarathonUsers;
        if (users != null) users.ApplySorting(options.OrderBy);

        var paginationModel = new Pagination<MarathonUsersPutViewModel, MarathonUsers>(c => _mapper.Map<MarathonUsersPutViewModel>(c), users, options);
        return ActionMethodResult.Success(paginationModel);
    }
}