﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Marathon.Migrations
{
    /// <inheritdoc />
    public partial class isPaid : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "MarathonUsers");

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "MarathonUsers",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "MarathonUsers");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "MarathonUsers",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
