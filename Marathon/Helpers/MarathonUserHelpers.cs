﻿using Marathon.Migrations;
using Marathon.Models.MarathonUsers;

namespace Marathon.Helpers;

public static class MarathonUserHelpers
{
    public static IQueryable<MarathonUsers> ApplySorting(this IQueryable<MarathonUsers> source, MarathonUsersSortState sortState)
    {
        return sortState switch
        {
            MarathonUsersSortState.IdAsc => source.OrderBy(x => x.Id),
            MarathonUsersSortState.IdDesc => source.OrderByDescending(x => x.Id),
            MarathonUsersSortState.EmailAsc => source.OrderBy(x => x.Email),
            MarathonUsersSortState.EmailDesc => source.OrderByDescending(x => x.Email),
            MarathonUsersSortState.PhoneNumberAsc => source.OrderBy(x => x.PhoneNumber),
            MarathonUsersSortState.PhoneNumberDesc => source.OrderByDescending(x => x.PhoneNumber),
            MarathonUsersSortState.FirstNameAsc => source.OrderBy(x => x.FirstName),
            MarathonUsersSortState.FirstNameDesc => source.OrderByDescending(x => x.FirstName),
            MarathonUsersSortState.LastNameAsc => source.OrderBy(x => x.LastName),
            MarathonUsersSortState.LastNameDesc => source.OrderByDescending(x => x.LastName),
            MarathonUsersSortState.GenderNameAsc => source.OrderBy(x => x.GenderName),
            MarathonUsersSortState.GenderNameDesc => source.OrderByDescending(x => x.GenderName),
            MarathonUsersSortState.CountryNameAsc => source.OrderBy(x => x.CountryName),
            MarathonUsersSortState.CountryNameDesc => source.OrderByDescending(x => x.CountryName),
            MarathonUsersSortState.CityNameAsc => source.OrderBy(x => x.CityName),
            MarathonUsersSortState.CityNameDesc => source.OrderByDescending(x => x.CityName),
            MarathonUsersSortState.CreatedDateAsc => source.OrderBy(x => x.CreatedDate),
            MarathonUsersSortState.CreatedDateDesc => source.OrderByDescending(x => x.CreatedDate),
            _ => source,
        };
    }

    public enum MarathonUsersSortState
    {
        IdAsc, CreatedDateAsc, EmailAsc, PhoneNumberAsc, FirstNameAsc, LastNameAsc, GenderNameAsc, CountryNameAsc, CityNameAsc,
        IdDesc, CreatedDateDesc, EmailDesc, PhoneNumberDesc, FirstNameDesc, LastNameDesc, GenderNameDesc, CountryNameDesc, CityNameDesc
    }
}