﻿namespace Marathon.Exceptions;

public class UserCreatingException : Exception
{
    public UserCreatingException() { }

    public UserCreatingException(string message) : base(message) { }

    public UserCreatingException(string message, Exception innerException) : base(message, innerException) { }
}