﻿// using System.Reflection;
// using Marathon.Middleware;
// using Marathon.Models;
// using Marathon.Repositories;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Mvc.ApiExplorer;
// using Microsoft.AspNetCore.Mvc.Versioning;
// using Microsoft.EntityFrameworkCore;
// using Microsoft.Extensions.Options;
// using Microsoft.OpenApi.Models;
// using Newtonsoft.Json.Converters;
// using Swashbuckle.AspNetCore.SwaggerGen;
//
// namespace Marathon;
//
// public class Startup
// {
//     public Startup(IConfiguration configuration)
//     {
//         Configuration = configuration;
//     }
//
//     private IConfiguration Configuration { get; }
//
//     public void ConfigureServices(IServiceCollection services)
//     {
//         services.AddMvcCore().AddApiExplorer();
//         services.AddControllers().AddNewtonsoftJson(jsonOptions =>
//         {
//             jsonOptions.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
//             jsonOptions.SerializerSettings.Converters.Add(new StringEnumConverter());
//         });
//         services.AddApiVersioning(s =>
//         {
//             // api-deprecated-versions
//             // api-supported-versions
//             s.ApiVersionReader = new HeaderApiVersionReader("api-version");
//             s.AssumeDefaultVersionWhenUnspecified = true;
//             s.DefaultApiVersion = new ApiVersion(1, 0);
//         });
//
//         // services.AddOptions();
//         // services.AddSwaggerGen(c =>
//         // {
//         //     c.SwaggerDoc("v1", new OpenApiInfo { Title = "Marathon", Version = "v1" });
//         // });
//
//
//         string? appDbConnection = Configuration["ConnectionStrings:AppDbConnection"];
//         services.AddDbContext<AppDbContext>(options =>
//                 options.UseNpgsql(appDbConnection,
//                         b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName))
//                     .UseLazyLoadingProxies()
//             , ServiceLifetime.Transient);
//         services.AddScoped<IAppDbContext>(provider => provider.GetService<AppDbContext>());
//         services.AddTransient<IMarathonUserRepository, MarathonUserRepository>();
//         services.AddHttpContextAccessor();
//         services.AddAutoMapper(typeof(Startup).Assembly);
//         services.AddVersionedApiExplorer(options =>
//         {
//             // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
//             // note: the specified format code will format the version as "'v'major[.minor][-status]"
//             options.GroupNameFormat = "'v'VVV";
//
//             // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
//             // can also be used to control the format of the API version in route templates
//             options.SubstituteApiVersionInUrl = true;
//         });
//         services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
//         services.AddSwaggerGen(options => { options.DocumentFilter<SwaggerFilterOutControllers>(); });
//     }
//
//
//     public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
//     {
//         if (env.IsDevelopment())
//         {
//             app.UseDeveloperExceptionPage();
//         }
//
//         var environment = Configuration["Environment"];
//         if (environment != "production") // TODO: disabled for allow upload users
//         {
//             app.UseSwagger();
//             app.UseSwaggerUI();
//         }
//
//         app.UseCustomExceptionHandler();
//
//         app.UseCors(builder => builder
//             .WithOrigins(Configuration.GetSection("Origins").Get<string[]>())
//             // .AllowAnyOrigin()
//             .AllowCredentials()
//             .AllowAnyHeader()
//             .AllowAnyMethod()
//         );
//
//         // app.UseHttpsRedirection();
//         app.UseRouting();
//         app.UseStatusCodePages();
//         app.UseAuthentication();
//         app.UseAuthorization();
//
//         app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
//
//         // RecurringJob.AddOrUpdate(() => contractUpdateService.SendContractsExpirationNotification(), "0 9 * * *", TimeZoneInfo.Local);
//     }
//
//     public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
//     {
//         private readonly IApiVersionDescriptionProvider _provider;
//
//         public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
//         {
//             _provider = provider;
//         }
//
//         public void Configure(SwaggerGenOptions options)
//         {
//             // add a swagger document for each discovered API version
//             // note: you might choose to skip or document deprecated API versions differently
//             foreach (var description in _provider.ApiVersionDescriptions)
//             {
//                 options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
//             }
//
//             // Set the comments path for the Swagger JSON and UI.
//             var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
//             var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
//             options.IncludeXmlComments(xmlPath);
//         }
//
//         private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
//         {
//             var info = new OpenApiInfo()
//             {
//                 Title = "Marathon",
//                 Version = description.ApiVersion.ToString(),
//             };
//
//             if (description.IsDeprecated)
//             {
//                 info.Description += " This API version has been deprecated.";
//             }
//
//             return info;
//         }
//     }
//
//     public class SwaggerFilterOutControllers : IDocumentFilter
//     {
//         public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
//         {
//             var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
//
//             if (env == "Development")
//                 return;
//
//             if (env == "Test")
//                 return;
//
//             // if (env == "Production")
//             //     return;
//
//             // var allowPaths = new List<string>() { "/_Scripts" };
//             // List<string> pathKeysToRemove = swaggerDoc.Paths.Where(x => !allowPaths.Any(e => x.Key.Contains(e))).Select(x => x.Key).ToList();
//             // foreach (var pathKey in pathKeysToRemove)
//             // {
//             //     swaggerDoc.Paths.Remove(pathKey);
//             // }
//         }
//     }
// }