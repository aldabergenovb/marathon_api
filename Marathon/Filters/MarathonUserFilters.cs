﻿using Marathon.Helpers;
using Marathon.Models.Directories;

namespace Marathon.Filters;

public class MarathonUserFilters : BasePaginationOptions<MarathonUserHelpers.MarathonUsersSortState>
{
    public MarathonUserFilters() : base(MarathonUserHelpers.MarathonUsersSortState.CreatedDateDesc) { }
    
}