﻿using System.Net;
using System.Net.Mail;

namespace Marathon.Services;

public class EmailSender
{
    public void SendEmail(string recipientEmail, string subject, string body)
    {
        // Настройки SMTP-сервера
        string smtpServer = "smtp.mail.ru";
        int smtpPort = 465;
        string smtpUsername = "aldabergenovb@mail.ru";
        string smtpPassword = "G1N1C3dxjut9b9GnVmr0";

        // Формирование письма
        MailMessage message = new MailMessage();
        message.From = new MailAddress("aldabergenovb@mail.ru");
        message.To.Add(new MailAddress("aldabergenovb@gmail.com"));
        message.Subject = "subject";
        message.Body = "body";

        // Настройка клиента SMTP и отправка письма
        SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort);
        smtpClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
        smtpClient.EnableSsl = false; // Если ваш SMTP-сервер использует SSL, иначе установите в false

        try
        {
            smtpClient.Send(message);
            Console.WriteLine("Письмо успешно отправлено!");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Ошибка при отправке письма: " + ex.Message);
        }
    }
}