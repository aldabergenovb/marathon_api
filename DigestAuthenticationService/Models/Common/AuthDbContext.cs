﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DigestAuthenticationService.Models.Common;

public class AuthDbContext : DbContext
{
    public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options) { }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.EnableSensitiveDataLogging();

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        // configurationBuilder
        //     .Properties<DateTimeOffset>()
        //     .HaveConversion<DateTimeOffsetConverter>();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ConfigureAppDb();
    }
    public DbSet<EsbUser> EsbUsers { get; set; } = null!;
}

public abstract class DateTimeOffsetConverter : ValueConverter<DateTimeOffset, DateTimeOffset>
{
    protected DateTimeOffsetConverter() : base(d => d.ToUniversalTime(), d => d.ToUniversalTime()) { }
}