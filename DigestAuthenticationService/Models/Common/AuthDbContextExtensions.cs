﻿using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Models.Common;

public static class AuthDbContextExtensions
{
    public static void ConfigureAppDb(this ModelBuilder builder)
    {
        builder.Entity<EsbUser>()
            .HasKey(x => x.Id);
    }
}