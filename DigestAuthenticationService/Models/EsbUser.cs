﻿using DigestAuthenticationService.Models.Common;
using Microsoft.AspNetCore.Identity;

namespace DigestAuthenticationService.Models;

public class EsbUser
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string? UserName { get; set; } = null!;
    public string PasswordHash { get; set; } = null!;
    public string? SecretKey { get; set; } = null!;
    ///<summary>Наименование системы</summary>
    public string? SystemName { get; set; }
    ///<summary>IP-адрес системы</summary>
    public string? SystemIpAddress { get; set; }
    ///<summary>БИН Организации</summary>
    public string? OrganizationBIN { get; set; }
    ///<summary>Наименование Организации</summary>
    public string? OrganizationName { get; set; }
    ///<summary>ФИО Ответственного лица</summary>
    public string? OrganizationRespresenter { get; set; }
    ///<summary>Контакты Ответственного лица</summary>
    public string? OrganizationRespresenterPhone { get; set; }
}

public class EsbUserCreateViewModel
{
    public string? UserName { get; set; }
    public string? SecretKey { get; set; }
    public string? SystemName { get; set; }
    public string? SystemIpAddress { get; set; }
    public string? OrganizationBIN { get; set; }
    public string? OrganizationName { get; set; }
    public string? OrganizationRespresenter { get; set; }
    public string? OrganizationRespresenterPhone { get; set; }
}

public class EsbUserViewModel
{
    public Guid Id { get; set; } 
    public string? UserName { get; set; }
    public string? SecretKey { get; set; }
    public string? SystemName { get; set; }
    public string? SystemIpAddress { get; set; }
    public string? OrganizationBIN { get; set; }
    public string? OrganizationName { get; set; }
    public string? OrganizationRespresenter { get; set; }
    public string? OrganizationRespresenterPhone { get; set; }
}