﻿using AutoMapper;

namespace DigestAuthenticationService.Models;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<EsbUser, EsbUserCreateViewModel>();
        CreateMap<EsbUser, EsbUserViewModel>();
    }
}