﻿using DigestAuthenticationService.Helpers;
using DigestAuthenticationService.Models.Directories;

namespace DigestAuthenticationService.Filters;

public class UserFilters : BasePaginationOptions<UsersHelper.AppUsersSortState>
{
    public UserFilters() : base(UsersHelper.AppUsersSortState.UserNameAsc)
    {
    }

    public string? UserName { get; set; }
    public string? SystemName { get; set; }
    public string? OrganizationBIN { get; set; }
    public string? OrganizationName { get; set; }
    public string? OrganizationRespresenter { get; set; }
}