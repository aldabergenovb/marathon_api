﻿using DigestAuthenticationService.Filters;
using DigestAuthenticationService.Models;
using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Helpers;

public static class UsersHelper
{
    public static IQueryable<EsbUser> ApplyFilters(this IQueryable<EsbUser> source, UserFilters options)
    {
        // if (options.StartCreated != null)
        //     source = source.Where(x => x.Created >= options.StartCreated);
        return source;
    }

    public static IQueryable<EsbUser> ApplySorting(this IQueryable<EsbUser> source, AppUsersSortState sortState)
    {
        return sortState switch
        {
            AppUsersSortState.UserNameAsc => source.OrderBy(x => x.UserName),
            AppUsersSortState.UserNameDesc => source.OrderByDescending(x => x.UserName),
            AppUsersSortState.SystemNameAsc => source.OrderBy(x => x.SystemName),
            AppUsersSortState.SystemNameDesc => source.OrderByDescending(x => x.SystemName),
            AppUsersSortState.OrganizationNameAsc => source.OrderBy(x => x.OrganizationName),
            AppUsersSortState.OrganizationNameDesc => source.OrderByDescending(x => x.OrganizationName),
            AppUsersSortState.OrganizationBinAsc => source.OrderBy(x => x.OrganizationBIN),
            AppUsersSortState.OrganizationBinDesc => source.OrderByDescending(x => x.OrganizationBIN),
            AppUsersSortState.OrganizationRespresenterAsc => source.OrderBy(x => x.OrganizationRespresenter),
            AppUsersSortState.OrganizationRespresenterDesc => source.OrderByDescending(x => x.OrganizationRespresenter),
            _ => source,
        };
    }

    public enum AppUsersSortState
    {
        UserNameAsc,
        UserNameDesc,
        SystemNameAsc,
        SystemNameDesc,
        OrganizationNameAsc,
        OrganizationNameDesc,
        OrganizationBinAsc,
        OrganizationBinDesc,
        OrganizationRespresenterAsc,
        OrganizationRespresenterDesc
    }
}