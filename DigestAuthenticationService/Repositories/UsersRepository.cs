﻿using System.Security.Claims;
using AutoMapper;
using DigestAuthenticationService.Constants;
using DigestAuthenticationService.Filters;
using DigestAuthenticationService.Helpers;
using DigestAuthenticationService.Models;
using DigestAuthenticationService.Models.Common;
using DigestAuthenticationService.Models.Directories;
using FlakeyBit.DigestAuthentication.Implementation;
using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Repositories;

public interface IUsersRepository
{
    Task<ActionMethodResult> GetAsync(Guid userId);
    Task<ActionMethodResult> GetAllPaginatedAsync(UserFilters options);
    Task<ActionMethodResult> PutAsync(EsbUserViewModel model);
    Task<ActionMethodResult> CreateAsync(EsbUserCreateViewModel model);
    Task<ActionMethodResult> RemoveUserAsync(Guid id);
}

public class UsersRepository : IUsersRepository
{
    private readonly AuthDbContext _db;
    private readonly IMapper _mapper;

    public UsersRepository(AuthDbContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }

    public async Task<ActionMethodResult> GetAsync(Guid userId)
    {
        var user = await _db.EsbUsers.FirstOrDefaultAsync(x=>x.Id == userId);
        if (user is null)
            return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "Пользователь не найден");

        var viewModel = _mapper.Map<EsbUserCreateViewModel>(user);

        return ActionMethodResult.Success(viewModel);
    }
    
    public Task<ActionMethodResult> GetAllPaginatedAsync(UserFilters options)
    {
        IQueryable<EsbUser> users = _db.EsbUsers;
        users = users.ApplyFilters(options);
        users = users.ApplySorting(options.OrderBy);
        // var viewModel = _mapper.Map<AppUserCreateViewModel>(user);
        var paginationModel = new Pagination<EsbUserViewModel, EsbUser>(c => _mapper.Map<EsbUserViewModel>(c), users, options);
        return Task.FromResult(ActionMethodResult.Success(paginationModel));
    }

    public async Task<ActionMethodResult> PutAsync(EsbUserViewModel model)
    {
        var user = await _db.EsbUsers.FirstOrDefaultAsync(x=>x.Id == model.Id);
        if (user is null)
            return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "Пользователь не найден");
        
        user.OrganizationName = model.OrganizationName;
        user.OrganizationBIN = model.OrganizationBIN;
        user.OrganizationRespresenter = model.OrganizationRespresenter;
        user.OrganizationRespresenterPhone = model.OrganizationRespresenterPhone;
        user.SystemName = model.SystemName;
        user.SystemIpAddress = model.SystemIpAddress;
        
        if (user.SecretKey != model.SecretKey || user.UserName != model.UserName)
        {
            var hash = DigestAuthentication.ComputeA1Md5Hash(user.UserName, "some-realm", model.SecretKey);
            user.UserName = model.UserName;
            user.PasswordHash = hash;
            user.SecretKey = model.SecretKey;
        }
        
        _db.EsbUsers.Update(user);
        await _db.SaveChangesAsync();
        
        return ActionMethodResult.Success(null, "Данные успешно сохранены");
    }

    public async Task<ActionMethodResult> CreateAsync(EsbUserCreateViewModel model)
    {
        if (string.IsNullOrEmpty(model.SecretKey) || string.IsNullOrEmpty(model.UserName))
            return ActionMethodResult.Error("Ошибка", "Заполните поля Имя пользователя и Секретный ключ");
        var hash = DigestAuthentication.ComputeA1Md5Hash(model.UserName, "some-realm", model.SecretKey);
        EsbUser user = new()
        {
            OrganizationName = model.OrganizationName,
            OrganizationRespresenter = model.OrganizationRespresenter,
            SecretKey = model.SecretKey,
            SystemName = model.SystemName,
            OrganizationRespresenterPhone = model.OrganizationRespresenterPhone,
            SystemIpAddress = model.SystemIpAddress,
            OrganizationBIN = model.OrganizationBIN,
            UserName = model.UserName,
            PasswordHash = hash
        };
        await _db.EsbUsers.AddAsync(user);
        await _db.SaveChangesAsync();
        return ActionMethodResult.Success(user.Id, "Данные успешно сохранены");
    }

    public async Task<ActionMethodResult> RemoveUserAsync(Guid id)
    {
        var user = await _db.EsbUsers.FirstOrDefaultAsync(x=>x.Id == id);
        if (user is null)
            return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "Пользователь не найден");
        _db.EsbUsers.Remove(user);
        await _db.SaveChangesAsync();
        return ActionMethodResult.Success(null, "Данные успешно удалены");
    }
}