﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigestAuthenticationService.Migrations
{
    public partial class UserSecretKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SecretKey",
                table: "AppUsers",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecretKey",
                table: "AppUsers");
        }
    }
}
