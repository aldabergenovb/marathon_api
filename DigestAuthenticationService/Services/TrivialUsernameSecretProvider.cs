﻿using System.Configuration;
using DigestAuthenticationService.Models.Common;
using FlakeyBit.DigestAuthentication.Implementation;
using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Services;

/// <summary>
/// Digest Авторизация по логину паролю
/// </summary>
internal class TrivialUsernameSecretProvider : IUsernameSecretProvider
{
    private readonly AuthDbContext _db;
    private readonly IConfiguration _configuration;

    public TrivialUsernameSecretProvider(AuthDbContext db, IConfiguration configuration)
    {
        _db = db;
        _configuration = configuration;
    }
    public async Task<string> GetSecretForUsernameAsync(string username)
    {
        try
        {
            var login = "test";
            var password = "test";
            if (username == login) {
                return await Task.FromResult(password);
            }

            return await Task.FromResult<string>(null);
        }
        catch (Exception e)
        {
            return await Task.FromResult<string>(e.Message);
        }
        
    }
}