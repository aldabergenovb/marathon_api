﻿using DigestAuthenticationService.Models.Common;
using FlakeyBit.DigestAuthentication.Implementation;
using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Services;

/// <summary>
/// Digest авторизация с проверкой хэша MD5
/// </summary>
internal class TrivialUsernameHashedSecretProvider : IUsernameHashedSecretProvider
{
    private readonly AuthDbContext _db;

    public TrivialUsernameHashedSecretProvider(AuthDbContext db)
    {
        _db = db;
    }

    public async Task<string> GetA1Md5HashForUsernameAsync(string username, string realm)
    {
        var user = await _db.EsbUsers.FirstOrDefaultAsync(u => u.UserName == username);
        if (user != null)
        {
            if (!string.IsNullOrEmpty(user.PasswordHash))
                return await Task.FromResult(user.PasswordHash);
        }
        // User not found
        return await Task.FromResult<string>(null!);
    }
}