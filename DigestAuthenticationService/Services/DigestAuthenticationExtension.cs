﻿using DigestAuthenticationService.Models.Common;
using FlakeyBit.DigestAuthentication.AspNetCore;
using FlakeyBit.DigestAuthentication.Implementation;
using Microsoft.EntityFrameworkCore;

namespace DigestAuthenticationService.Services;

public static class DigestAuthenticationExtension

{
    public static void AddDigestAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<AuthDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("AuthConnection")));
        // авторизация по логину паролю
        services.AddScoped<IUsernameSecretProvider, TrivialUsernameSecretProvider>();
        services.AddAuthentication("Digest")
            .AddDigestAuthentication(DigestAuthenticationConfiguration.Create("VerySecret", "some-realm", 60, true, 20));
    }
}