using AutoMapper.Configuration.Annotations;
using DigestAuthenticationService.Constants;
using DigestAuthenticationService.Filters;
using DigestAuthenticationService.Models;
using DigestAuthenticationService.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigestAuthenticationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersRepository _repo;
        // GET: api/User
        public UsersController(IUsersRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Получение пользователя по Id
        /// </summary>
        /// <param name="id"></param>
        [HttpGet("{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(AuthenticationSchemes = "Digest")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            ActionMethodResult result;
            if (!id.Equals(Guid.Empty))
                result = await _repo.GetAsync(id);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "Некорректные данные");
            return Ok(result);
        }
        
        /// <summary>
        /// Получение списка пользователей
        /// </summary>
        /// <param name="options"></param>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(AuthenticationSchemes = "Digest")]
        public async Task<IActionResult> GetAllPaginatedAsync([FromQuery] UserFilters options)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _repo.GetAllPaginatedAsync(options);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "Некорректные данные");
            return Ok(result);
        }

        // POST: api/User
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(AuthenticationSchemes = "Digest")]
        public async Task<IActionResult> Post([FromBody] EsbUserCreateViewModel model)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _repo.CreateAsync(model);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "Некорректные данные");
            return Ok(result);
        }

        // PUT: api/User/5
        [HttpPut]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(AuthenticationSchemes = "Digest")]
        public async Task<IActionResult> Put([FromBody] EsbUserViewModel model)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _repo.PutAsync(model);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "Некорректные данные");
            return Ok(result);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(AuthenticationSchemes = "Digest")]
        public async Task<IActionResult> Delete(Guid id)
        {
            ActionMethodResult result;
            if (id.Equals(Guid.Empty))
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "Некорректные данные");
            else
                result = await _repo.RemoveUserAsync(id);
            return Ok(result);
        }
    }
}
